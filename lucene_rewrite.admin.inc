<?php


/**
 * @file
 * Admistration settings for Lucene Rewrite modules
 */

/**
 * Settings form
 */
function lucene_rewrite_settings() {
  $form = array(
    'lucene_rewrite' => array(
      '#type'        => 'fieldset',
      '#title'       => t('Settings'),
      '#description' => t('Configure the search string rewrite rules for the Lucene Search. '.
                          'More rule can be added after you save current rules.'.
                          '<p><b>Pattern</b> is a regular expression pattern (without leading '.
                          'and trailing slash) to be applied to the query. If no matching is '.
                          'found, it will advance to the next rule automatically.</p>'.
                          '<p><b>Replacement</b> is the regular expression replacement pattern '.
                          'as used by the PHP\'s <em>preg_replace()</em> function, i.e.,<br/>'.
                          '<ul>'.
                          '<li><em>$0</em> : refer to the text matched by the whole pattern</li>'.
                          '<li><em>$1 ...</em> : refer to the first parenthesised pattern and so on..</li>'.
                          '</ul>'.
                          '</p>'.
                          '<p>If Token module is installed, you can then utilise the token string '.
                          'as the part of both pattern and replacement. The token will be processed '.
                          'prior than the regular expression replacement.</p>'.
                          '<p>For more details about Lucene Query Language, please refer to '.
                          '<a href="http://drupal.org/node/375446" target="_blank">this article.</a>'.
                          '</p>'),
      '#collapsible' => FALSE,
      'rules' => array(
        '#id'     => 'lucene-rewrite-rules',
        '#theme'  => 'lucene_rewrite_rules',
        '#header' => array( t('Pattern'), t('Replacement'), ''),
      ),
    ),
  );

  $id = 0;
  $rules = array_merge(variable_get('lucene_rewrite.rules', array()), array('' => ''));
  foreach ( $rules as $pattern => $replace ) {
    $form['lucene_rewrite']['rules']['rules_'.$id] = array(
      '#prefix' => '<div class="lucene-rewrite-rule">',
      "pattern-$id" => array(
        '#type'          => 'textfield',
        '#default_value' => $pattern,
        '#size'          => 50,
        '#maxlength'     => 256,
        '#attributes'    => array('class' => 'lucene-rewrite-pattern'),
      ),
      "replace-$id" => array(
        '#type'          => 'textfield',
        '#default_value' => $replace,
        '#size'          => 50,
        '#maxlength'     => 256,
        '#attributes'    => array('class' => 'lucene-rewrite-replacement'),
      ),
      "weight-$id"  => array(
        '#type'          => 'weight',
        '#delta'         => $id,
        '#default_value' => $id,
        '#attributes' => array('class' => 'rule-weight'),
      ),
      '#suffix' => '</div>',
    );
    $id++;
  }

  $form['lucene_rewrite']['default'] = array(
    '#prefix' => '<div class="lucene-default-rewrite-rule lucene-rewrite-rule">',
    'replace-default'  => array(
      '#type'          => 'textfield',
      '#title'         => t('Default Replacement'),
      '#description'   => t('The default replacement pattern will apply to the query if the rules above '.
                            'cannot be applied. Use <em>$0</em> to refer to the query string.'),
      '#default_value' => variable_get('lucene_rewrite.default_rule', '"$0*"'),
      '#size'          => 110,
      '#maxlength'     => 256,
      '#attributes'    => array('class' => 'lucene-rewrite-replacement'),
    ),
    '#suffix' => '</div>',
  );

  if ( $index = luceneapi_index_open('luceneapi_node', $errstr) ) {
    $keys   = array_keys(luceneapi_index_fields_get($index, TRUE));
    $fields = (!empty($keys)) ? array_combine($keys, array_fill(0, count($keys), 0)) : array();

    // Finds the number of terms for each field in the index.
    $terms = luceneapi_index_terms_get($index, TRUE);
    $term_count = count($terms);
    //file_put_contents('/tmp/luceneindex.txt', print_r($terms, TRUE));
    foreach ($terms as $term) {
      $fields[$term->field]++;
    }

    // builds table
    $headers = array(
      t('Lucene field'),
      t('Number of unique terms')
    );
    $rows = array();
    foreach ($fields as $field => $num) {
      $rows[] = array(check_plain($field), check_plain($num));
    }

    $form['field_help'] = array(
      '#title'       => t('Available Lucene fields'),
      '#type'        => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
      '#description' => t('Available lucene fields those can be used with the rewrite rules.'),
      'help' => array(
        '#value' => theme('table', $headers, $rows),
      ),
    );
  }
  else {
    drupal_set_message($errstr, 'error');
  }

  if ( module_exists('token') ) {
    $form['token_help'] = array(
      '#title'       => t('Available tokens'),
      '#type'        => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
      '#description' => t('Available tokens those can be used with the rewrite rules.'),
      'help' => array(
        '#value' => theme('token_help', 'global'),
      ),
    );
  }

  $form['save'] = array(
    '#type'     => 'submit',
    '#validate' => array('lucene_rewrite_settings_validate'),
    '#submit'   => array('lucene_rewrite_settings_submit'),
    '#value'    => t('Save'),
  );

  return $form;
}

/**
 * theme the dragable rule fields
 */
function theme_lucene_rewrite_rules($element) {
  drupal_add_tabledrag($element['#id'], 'order', 'sibling', 'rule-weight');
  $rows = array();
  foreach ( element_children($element) as $key ) {
    $rr = array();
    $e  = $element[$key];
    foreach ( element_children($e) as $k ) {
      unset($e[$k]['#printed']);
      $rr[] = drupal_render($e[$k]);
    }
    $rows[] = array('data' => $rr, 'class' => 'draggable');
  }
  return theme('table', $element['#header'], $rows, array('id' => $element['#id']));
}

/**
 * Validate the rule
 */
function lucene_rewrite_settings_validate($form, &$form_state) {
  $id = 0;
  while ( isset($values["pattern-$id"]) || isset($values["replace-$id"]) ) {
    if ( empty($values["pattern-$id"]) xor empty($values["replace-$id"]) ) {
      form_set_error('replace-$id', t('Both pattern and replacement are required together.'));
    }
    $id++;
  }
  $default = trim($form_state['values']['replace-default']);
  if ( empty($default) ) {
    form_set_error('replace-default', t('Default replacemente pattern cannot be empty. You may use <em>$0</em> '.
                                        'to keep the query unchanged.'));
  }
}

/**
 * Setting submission
 */
function lucene_rewrite_settings_submit($form, &$form_state) {
  $rules  = array();
  $id = 0;

  $values = $form_state['values'];
  while ( isset($values["pattern-$id"]) && isset($values["replace-$id"]) ) {
    if ( empty($values["pattern-$id"]) && empty($values["replace-$id"]) ) {
      $id++;
      continue;
    }
    $rules[$values["weight-$id"]] = array($values["pattern-$id"] => $values["replace-$id"]);
    $id++;
  }

  ksort($rules);
  $settings = array();
  foreach ( $rules as $w => $r ) {
    $settings = array_merge($settings, $r);
  }

  variable_set('lucene_rewrite.rules', $settings);
  variable_set('lucene_rewrite.default_rule', $values['replace-default']);

  $cid = sprintf('%s:', 'luceneapi_node');
  cache_clear_all($cid, LUCENEAPI_CACHE_TABLE, TRUE);

  drupal_set_message('Lucene Rewrite settings are successfully saved.');
}