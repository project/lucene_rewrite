<?php

/**
 * @file
 * Lucene Rewrite Module - allow administer to rewrite the user query.
 */

// ----------------------------------------------------------------------------
// LuceneAPI Interfaces

function lucene_rewrite_luceneapi_query_rebuild($query, $module, $type = NULL) {
  static $being_rebuilt = FALSE;

  if ( $being_rebuilt ) {
    return $query;
  }

  $str = search_get_keys();

  // check, if current keyword is using Lucene Query Language
  for ( $i = 0; $i < strlen($str); $i++ ) {
    if ( $str[$i] == '\\' ) {
      $i++;
      continue;
    }
    if ( strpos(':?*~"[]{}^+-()', $str[$i]) !== FALSE ) {
      return $query;
    }
  }

  foreach ( explode(' ', $str) as $w ) {
    if ( in_array($w, array('AND', 'OR', 'NOT'))) {
      return $query;
    }
  }

  // select the best condition
  $rules = array_merge(variable_get('lucene_rewrite.rules', array()), array('^.*$' => variable_get('lucene_rewrite.default_rule', '"$0*"')));
  foreach ( $rules as $pattern => $replace ) {
    $pattern = token_replace($pattern);
    $replace = token_replace($replace);
    if ( ($new_str = preg_replace("/{$pattern}/", $replace, $str)) != $str ) {
      $str = $new_str;
      break;
    }
  }

  // rewrite the query
  $being_rebuilt = TRUE;
  $query = luceneapi_query_parse($str, $module);
  $being_rebuilt = FALSE;
  return $query;
}

// ----------------------------------------------------------------------------
// Drupal Interfaces

/**
 * Implementation of hook_menu().
 */
function lucene_rewrite_menu() {
  return array(
    'admin/settings/lucene_rewrite' => array(
      'title' => 'Search Lucene Query Rewrite',
      'description' => 'Configure the Lucene Search rewrite rules.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('lucene_rewrite_settings'),
      'access arguments' => array('administer site configuration'),
      'file' => 'lucene_rewrite.admin.inc',
    ),
  );
}

/**
 * Implementation of hook_theme().
 */
function lucene_rewrite_theme() {
  return array(
    'lucene_rewrite_rules' => array(
      'arguments' => array('element' => NULL),
      'file'      => 'lucene_rewrite_admin.inc',
    ),
  );
}